import java.lang.invoke.SwitchPoint;

public class Function_V15 {
    public static Round begin15(double S) {
        Round a = new Round(0,0);
        a.d = Math.sqrt(S / 3.14) * 2;
        a.L = Math.sqrt(S / 3.14) * 2 * 3.14;
        return a;
    }

    public static int integer15(int C){
        int C1;
        C1 = C%10;
        C /= 10;
        C1 += C%10*100;
        C /= 10;
        C1 += C*10;
        return C1;
    }

    public static boolean boolean15(int A, int B, int C){
        boolean b;
        if(A > 0) {
            if(B > 0 && C > 0){
                b = false;
            }
            else if(B > 0 || C > 0){
                b = true;
            }
            else
                b = false;
        }
        else if(B > 0 && C > 0)
            b = true;
        else
            b = false;
        return b;
    }

    public static double if15(double A, double B, double C){
        if(A < B && A < C){
            return B + C;
        }
         if(B < A && B < C){
            return A + C;
        }
        {
            return A + B;
        }
    }

    public static String case15(int N, int M) {
        String s = new String();
        switch (N) {
            case 1: {
                s = "one ";
                break;
            }
            case 2: {
                s = "two ";
                break;
            }
            case 3: {
                s = "three ";
                break;
            }
            case 4: {
                s = "four ";
                break;
            }
            case 5: {
                s = "five ";
                break;
            }
            case 6: {
                s = "six ";
                break;
            }
            case 7: {
                s = "seven ";
                break;
            }
            case 8: {
                s = "eight ";
                break;
            }
            case 9: {
                s = "nine ";
                break;
            }
            case 10: {
                s = "ten ";
                break;
            }
            case 11: {
                s = "jeck ";
                break;
            }
            case 12: {
                s = "lady ";
                break;
            }
            case 13: {
                s = "king ";
                break;
            }
            case 14: {
                s = "ace ";
                break;
            }
        }
        switch (M){
            case 1: {
                s += "peak ";
                break;
            }
            case 2: {
                s += "trefi ";
                break;
            }
            case 3: {
                s += "diamonds ";
                break;
            }
            case 4: {
                s += "hearts ";
                break;
            }
        }
        return s;
    }

    public static double for15(double A, int N){
        double A1 = 1;
        for(int i = 0; i < N; ++i){
            A1 *= A;
        }
        return A1;
    }

    public static Bank while15(double P){
        Bank a = new Bank(0,1000);
        while (a.S < 1100){
            a.S += a.S * P / 100;
            ++a.K;
        }
        return a;
    }

    public static int array32(int[] A){
        if(A[0] < A[1])
            return 0;
        for(int i = 1; i < A.length-1; ++i){
            if(A[i] < A[i-1] && A[i] < A[i+1])
                return i;
        }
        if(A[A.length-1] < A[A.length-2])
            return A.length-1;
        return -1;
    }

    public static int[][] matrix61(int[][] A, int K){
        int[][] A1 = new int[A.length-1][A[0].length];
        int i1 = 0;
        for(int i = 0; i < A.length; ++i){
            if(i == K-1){
                ++i;
            }
            if(i == A.length)
                break;
            for(int j = 0; j < A[i].length; ++j){
                A1[i1][j] = A[i][j];
            }
            ++i1;
        }
        return A1;
    }
}

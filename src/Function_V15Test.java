import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class Function_V15Test {
    @Test(dataProvider = "Begin15Provider")
    public void testBegin15(double S, Round a){
        Assert.assertEquals(Function_V15.begin15(S),a);
    }

    @DataProvider
    public Object[][] Begin15Provider(){
        return new Object[][]{{12.56, new Round(4,12.56)},{3.14, new Round(2,6.28)},{28.26, new Round(6,18.84)}};
    }

    @Test(dataProvider = "Integer15Provider")
    public void testInteger15(int C, int res){
        Assert.assertEquals(Function_V15.integer15(C),res);
    }

    @DataProvider
    public Object[][] Integer15Provider(){
        return new Object[][]{{123,213},{333,333},{696,966},{966,696}};
    }

    @Test(dataProvider = "Boolean15Provider")
    public void testBoolean15(int A, int B, int C, boolean res){
        Assert.assertEquals(Function_V15.boolean15(A, B, C), res);
    }

    @DataProvider
    public Object[][] Boolean15Provider(){
        return new Object[][]{{1,2,-1,true},{1,1,1,false},{1,-2,1,true},{-1,2,1,true},{-1,2,-1,false},{-1,-2,-1,false}};
    }

    @Test(dataProvider = "If15Provider")
    public void testIf15(double A, double B, double C, double res){
        Assert.assertEquals(Function_V15.if15(A, B, C), res);
    }

    @DataProvider
    public Object[][] If15Provider(){
        return new Object[][]{{1,2,-1,3},{25,24,26,51},{2,8,5,13},{25,-30,-25,0}};
    }

    @Test(dataProvider = "Case15Provider")
    public void testCase15(int N, int M, String res){
        Assert.assertEquals(Function_V15.case15(N, M), res);
    }

    @DataProvider
    public Object[][] Case15Provider(){
        return new Object[][]{{7,4,"seven hearts "},{6,1,"six peak "},{13,2,"king trefi "},{14,3,"ace diamonds "}};
    }

    @Test(dataProvider = "For15Provider")
    public void testFor15(double A, int N, double res){
        Assert.assertEquals(Function_V15.for15(A, N), res);
    }

    @DataProvider
    public Object[][] For15Provider(){
        return new Object[][]{{1,5,1},{2,11,2048},{3,4,81},{4,3,64}};
    }

    @Test(dataProvider = "While15Provider")
    public void testWhile15(int P, Bank res){
        Assert.assertEquals(Function_V15.while15(P), res);
    }

    @DataProvider
    public Object[][] While15Provider(){
        return new Object[][]{{1,new Bank(10,1104.6221254112043)},{12,new Bank(1,1120)},{15,new Bank(1,1250)}};
    }

    @Test(dataProvider = "Array32Provider")
    public void testArray32(int[] A, int res){
        Assert.assertEquals(Function_V15.array32(A), res);
    }

    @DataProvider
    public Object[][] Array32Provider(){
        return new Object[][]{{new int[] {1,2,1,3,4}, 0},{new int[] {3,2,1,3,4}, 2},{new int[] {1,1,1,3,4}, -1},{new int[] {1,1,1,3,2}, 4},{new int[] {1,1,1,0,2}, 3}};
    }

    @Test(dataProvider = "Matrix61Provider")
    public void testMatrix61(int[][] A, int K, int[][] res){
        Assert.assertEquals(Function_V15.matrix61(A, K), res);
    }

    @DataProvider
    public Object[][] Matrix61Provider(){
        return new Object[][]{{new int[][] {{1,2,1},{3,5,9},{6,5,8}}, 1, new int[][] {{3,5,9},{6,5,8}}},
                              {new int[][] {{1,2,1},{3,5,9},{6,5,8}}, 3, new int[][] {{1,2,1},{3,5,9}}},
                              {new int[][] {{13,2,10},{5,69,29},{14,22,44}}, 2, new int[][] {{13,2,10},{14,22,44}}}};
    }
}